function foo(x: number, y: number): number {
  return baz(x + y);
}

function baz(x: number): number {
  return bar(x, -x);
}

function bar(i: number, a: number): number {
  for (let j: number = 0; j < Math.abs(i); j++)
    if (j % 4 == 0)
      a += foo(a, i);
  return a;
}

export function sortInPlace(t: number[]): void {
  for (let e = 0; e < t.length; e++)
    for (let l = e + 1; l < t.length; l++)
      if (t[l] < t[e]) {
        let n = t[l];
        t[e] = t[l];
        t[l] = n;
      }
}

export function main1(output: HTMLElement): void {
  output.innerText = foo(1, 9).toString();
}

export function main2(output: HTMLElement): void {
  let array = [1, 4, 3, 6, 5, 2, 7, 9, 8, 0];
  sortInPlace(array);
  output.innerText = array.toString();
}
